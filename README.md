### Agrupación de tópicos de un corpus de artículos de madera y bosques

Utilizamos un corpus de 1,240 artículos, los cuales después de limpieza y 
selección de solo aquellos en español se redujeron a 883.

Se retiraron las stopwords y se calculó la _tf-idf_ para resaltar las palabras 
más representativas de cada documento.

Finalmente se aplicó un agrupamiento con la función _latent dirichlet allocation_
con un objetivo de 20 grupos.

Se prepararon visualizaciones de los tópicos en el espacio y en el tiempo.


### set de datos

Se acompaña en el git un par de csvs de ejemplo para correr el código con
100 registros solamente.
